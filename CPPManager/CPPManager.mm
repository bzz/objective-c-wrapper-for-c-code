//
//  CPPManager.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 24/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "CPPManager.h"
#import "cpp_class.h"


@implementation CPPManager


- (id)init {
	if (self = [super init]) {
	}
	return self;
}

- (int)functionX:(int)x Y:(int)y {
	return cpp->function(x, y);
}

@end
