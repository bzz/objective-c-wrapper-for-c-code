//
//  cpp_class.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 24/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#ifndef __ArtStudioPalettes__cpp_class__
#define __ArtStudioPalettes__cpp_class__


struct CPPClass {
	CPPClass();
	int function(int x, int y);
};


#endif /* defined(__ArtStudioPalettes__cpp_class__) */
