//
//  CPPManager.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 24/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//


#import <Foundation/Foundation.h>

#if defined __cplusplus
class CPPClass;						// forward class declaration
#else
typedef struct CPPClass CPPClass;   // forward struct declaration
#endif


@interface CPPManager : NSObject
{
@private
    CPPClass* cpp;
}

- (int)functionX:(int)x Y:(int)y;

@end