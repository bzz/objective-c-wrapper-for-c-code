//
//  cpp_class.cpp
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 24/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#include "cpp_class.h"
#include <iostream>

CPPClass::CPPClass() {}

int CPPClass::function(int x, int y) {
	return x * y;
}
