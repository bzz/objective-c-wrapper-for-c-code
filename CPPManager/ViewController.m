//
//  ViewController.m
//  CPPManager
//
//  Created by Mikhail Baynov on 24/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "ViewController.h"
#import "CPPManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	CPPManager *cppManager = [CPPManager new];
	NSLog(@" %i", [cppManager functionX:7 Y:7]);
}

@end
